package info.pasupasu.properti.activity;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import info.pasupasu.properti.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginPlaceFragment extends Fragment {

    public LoginPlaceFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_place, container, false);
    }
}
